(function() {

  var APP = angular.module("myAPP",['angular-loading-bar','ngRoute','angular-carousel']);

    APP.controller("MainController",function($scope,$window) {
        $scope.carouselIndex = 0;
        this.images = [
            { src:'salon_1.gif', title:'Header cerfem' },
            { src:'salon_2.gif', title:'Patron lors d une convention' }
        ];

        this.aluInoxInfo = [
            { image : 'inox_01.jpg', sousTitre : 'Bac + Grille' },
            { image : 'inox_02.jpg', sousTitre : 'Facade 2P2 ou 3P2 sur mesure' },
            { image : 'inox_03.jpg', sousTitre : 'Four1P1' },
            { image : 'inox_04.jpg', sousTitre : 'Goulotte Inox 1' },
            { image : 'inox_05.jpg', sousTitre : 'Goulotte Inox 2' },
            { image : 'inox_06.jpg', sousTitre : 'Marche pied' },
            { image : 'inox_07.jpg', sousTitre : 'Pelle Inox' },
            { image : 'inox_08.jpg', sousTitre : 'Présentoires 1' },
            { image : 'inox_09.jpg', sousTitre : 'Présentoires 2' },
            { image : 'inox_10.jpg', sousTitre : 'Présentoirse 3' },
            { image : 'inox_11.jpg', sousTitre : 'Présentoirse 4' },
            { image : 'inox_12.jpg', sousTitre : 'Support grille' },
            { image : 'inox_13.jpg', sousTitre : 'Support sur roulettes 1' },
            { image : 'inox_14.jpg', sousTitre : 'Support sur roulettes 2' },
            { image : 'inox_15.jpg', sousTitre : 'Table aluminium + Plexi pliante 1' },
            { image : 'inox_16.jpg', sousTitre : 'Table aluminium + Plexi pliante 2' }
        ];

        this.references = {
            'Paris' : [
                {nom : 'Entreprise',                 departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Aux Délices de l\'Etoile',   departement : '75016',          lieu : 'Paris'},
                {nom : 'Aux Pêches Normands',        departement : '75010',          lieu : 'Paris'},
                {nom : 'Josephine',                  departement : '75020',          lieu : 'Paris'},
                {nom : 'La Bague de Kenza',          departement : '75011',          lieu : 'Paris'},
                {nom : 'Mr Delmontel',               departement : '75018',          lieu : 'Paris'},
                {nom : 'Mr Demoncy',                 departement : '75020',          lieu : 'Paris'},
                {nom : 'Mr Julien',                  departement : '75001',          lieu : 'Paris'},
                {nom : 'Mr Louvard',                 departement : '75008',          lieu : 'Paris'},
                {nom : 'Mr Secco',                   departement : '75007',          lieu : 'Paris'},
                {nom : 'Patisserie Carette',         departement : '75016',          lieu : 'Paris'},
                {nom : 'Patisserie Gaudard',         departement : '75020',          lieu : 'Paris'},
                {nom : 'Patisserie Malitourne',      departement : '75016',          lieu : 'Paris'},
                {nom : 'Sainte Tawak',               departement : '75013',          lieu : 'Paris'},
                {nom : 'Saint Sauveur',              departement : '75020',          lieu : 'Paris'}
            ],
            'Ile de France' : [
                {nom : 'Entreprise',                 departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Mr Arson',                   departement : '95',             lieu : 'Soisy sous Monmonrency'},
                {nom : 'Mr Bayart',                  departement : '77',             lieu : 'Quincy Voisins'},
                {nom : 'Aux Délices de Plaisance',   departement : '93',             lieu : 'Neuilly Plaisance'},
                {nom : 'Au Pere Petrin',             departement : '77',             lieu : 'Combs La Ville'},
                {nom : 'Mr Croenne',                 departement : '78',             lieu : 'Versailles'},
                {nom : 'Fournil de Morigny',         departement : '91',             lieu : 'Morigny champigny'},
                {nom : 'La Corbeille a Pain',        departement : '92',             lieu : 'La Garenne Colombe'},
                {nom : 'La Delicieuse',              departement : '92',             lieu : 'Malakoff'},
                {nom : 'Le Fournil de Mrigny',       departement : '77',             lieu : 'Fontainebleau'},
                {nom : 'Le Petit Duc',               departement : '94',             lieu : 'Saint Maur des Fosses'},
                {nom : 'Les 3 H - La Case a pain',   departement : '95',             lieu : 'Franconville'},
                {nom : 'Pauline',                    departement : '95',             lieu : 'Pontoise'},
                {nom : 'Mr Rachinel',                departement : '92',             lieu : 'Boulogne'},
                {nom : 'Rubis SARL',                 departement : '92',             lieu : 'Sceaux'}
            ],
            'La Province' : [
                {nom : 'Entreprise',                    departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Au Regal Breton',               departement : '56',             lieu : 'Auray'},
                {nom : 'Chocolaterie de la Tourelle',   departement : '51',             lieu : 'Nancy'},
                {nom : 'Mr Batt',                       departement : '44',             lieu : 'Nancy'},
                {nom : 'Boulangerie d\'Honore',         departement : '44',             lieu : 'Nantes'},
                {nom : 'Mr Bresson',                    departement : '71',             lieu : 'Bourbon Lancy'},
                {nom : 'Confiserie Genot',              departement : '54',             lieu : 'Nancy'},
                {nom : 'Mr Destombes',                  departement : '59',             lieu : 'Linselle'},
                {nom : 'La Bonbonniere',                departement : '31',             lieu : 'Toulouse'},
                {nom : 'La Lutine',                     departement : '85',             lieu : 'L\'orbrie'},
                {nom : 'Le Four a Bois',                departement : '79',             lieu : 'Parthenay'},
                {nom : 'Le Provencal',                  departement : '83',             lieu : 'Saint Raphael'},
                {nom : 'Patisserie le Trianon',         departement : '86',             lieu : 'Poitier'},
                {nom : 'Patisserie Naze',               departement : '83',             lieu : 'Barjols'},
                {nom : 'Pascalis',                      departement : '26',             lieu : 'Bourg de Peage'},
                {nom : 'Wach',                          departement : '67',             lieu : 'Selestat'}
            ],
            'Traiteur' : [
                {nom : 'Entreprise',                departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Charles Traiteur Prestige', departement : '93',             lieu : 'Montreuil'},
                {nom : 'Erizay Traiteur',           departement : '27',             lieu : 'Saint Aubin'},
                {nom : 'Lucullus d\'Auteuil',       departement : '75016',          lieu : 'Paris'},
                {nom : 'Mr Noblet',                 departement : '75014',          lieu : 'Paris'},
                {nom : 'Mr Menard',                 departement : '14',             lieu : 'Colombelles'}
            ],
            'Restauration et Hotellerie' : [
                {nom : 'Entreprise',                    departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Au Coeur de la Foret',          departement : '95',             lieu : 'Montmonrency'},
                {nom : 'Chez Casiir',                   departement : '75010',          lieu : 'Paris'},
                {nom : 'Chez Henri',                    departement : '93',             lieu : 'Romainville'},
                {nom : 'Chez Michel',                   departement : '75010',          lieu : 'Paris'},
                {nom : 'Dupont Versailles',             departement : '75015',          lieu : 'Paris'},
                {nom : 'Hotel Concorde Opera Paris',    departement : '75009',          lieu : 'Paris'},
                {nom : 'Hotel George V',                departement : '75008',          lieu : 'Paris'},
                {nom : 'Hotel Sofitel',                 departement : 'LA DEFENCE',     lieu : 'Paris'},
                {nom : 'La Ferme aux Frives',           departement : '40',             lieu : 'Eugenie les Bains'},
                {nom : 'La Fermette Marbeuf',           departement : '75008',          lieu : 'Paris'},
                {nom : 'La Madeleine',                  departement : '69',             lieu : 'Sens'},
                {nom : 'Le Moulin de la Gorce',         departement : '87',             lieu : 'Le Roche l\'Abeille'},
                {nom : 'Le Camelia',                    departement : '78',             lieu : 'Bougival'},
                {nom : 'Le Man Ray',                    departement : '75008',          lieu : 'Paris'},
                {nom : 'Le Marty',                      departement : '75005',          lieu : 'Paris'},
                {nom : 'Les Pres d\'Eugenie',           departement : '40',             lieu : 'Eugenie les Bains'},
                {nom : 'Pavillon d\'Auphine',           departement : '75016',          lieu : 'Paris'}
            ],
            'Professionnel et Cuisine' : [
                {nom : 'Entreprise',                        departement : 'Departement',    lieu : 'Lieu'},
                {nom : 'Campus Universitairede Bobigny',    departement : '93',             lieu : 'Bobigny'},
                {nom : 'Ceproc',                            departement : '75019',          lieu : 'Paris'},
                {nom : 'Cercle des Officiers',              departement : '78',             lieu : 'Versailles'},
                {nom : 'Cercle des Sous Officiers',         departement : '75013',          lieu : 'Paris'},
                {nom : 'Eal des Loges',                     departement : '78',             lieu : 'Saint Germain en Laye'},
                {nom : 'Ecole de Boulangerie',              departement : '75012',          lieu : 'Paris'},
                {nom : 'Ecoles des Metiers de la Table',    departement : '75017',          lieu : 'Paris'},
                {nom : 'Ecole Hotelliere Jean Drouant',     departement : '75017',          lieu : 'Paris'},
                {nom : 'Ecole Gregoire Ferrandi',           departement : '75006',          lieu : 'Paris'},
                {nom : 'INBP',                              departement : '76',             lieu : 'Rouen'},
                {nom : 'INSEP',                             departement : '75012',          lieu : 'Paris'},
                {nom : 'LCL',                               departement : '75002',          lieu : 'Paris'},
                {nom : 'Lycée Belliard',                    departement : '75018',          lieu : 'Paris'},
                {nom : 'Lycée Prof Hotellier',              departement : '05',             lieu : 'Tarbes'},
                {nom : 'Lycée d\'Enseignement Prof',        departement : '31',             lieu : 'Breuil le Vert'},
                {nom : 'Lycée Jean Monnet',                 departement : '87',             lieu : 'Limoges'},
                {nom : 'Racing Club de France',             departement : '75016',          lieu : 'Paris'},
                {nom : 'Rastaurant Universitaire',          departement : '31',             lieu : 'Toulouse'}
            ]
        };
    });

    APP.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/home'
            })
            .when('/historique', {
                templateUrl: 'partials/historique'
            })
            .when('/pointsForts', {
                templateUrl: 'partials/pointsForts'
            })
            .when('/fours', {
                templateUrl: 'partials/fours'
            })
            .when('/1etage',{
                templateUrl: 'partials/1etage'
            })
            .when('/2etage',{
                templateUrl: 'partials/2etage'
            })
            .when('/3etage',{
                templateUrl: 'partials/3etage'
            })
            .when('/4etage',{
                templateUrl: 'partials/4etage'
            })
            .when('/traiteur',{
                templateUrl: 'partials/traiteur'
            })
            .when('/mentions',{
                templateUrl: 'partials/mentions'
            })
            .when('/options',{
                templateUrl: 'partials/options'
            })
            .when('/sav',{
                templateUrl: 'partials/sav'
            })
            .when('/aluInox',{
                templateUrl: 'partials/aluInox'
            })
            .when('/reference',{
                templateUrl: 'partials/reference'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
})();
